import toml
import shutil

from flask import Flask, redirect, url_for, send_file

app = Flask(__name__)


@app.route('/ver')
def status():
    setup = toml.load('version.toml')
    return setup


@app.route('/')
def home():
    return redirect(url_for('status'), code=302)


@app.route('/files')
def files():
    shutil.make_archive('updates', 'zip', 'updates')
    return send_file('updates.zip', as_attachment=True, attachment_filename='updates.zip')


app.run(port=5404)
